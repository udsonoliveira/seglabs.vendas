import { Component } from '@angular/core';

import {SidebarService} from '../sidebar/sidebar.service'

@Component({
  selector: 'sl-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent{
  constructor(private sidebarService: SidebarService){
    
  } 
    
  toggleSidebar() {
    this.sidebarService.toggleClass();
  }
}
