import {BehaviorSubject, Observable} from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class SidebarService {
  
  private classApplied: BehaviorSubject<boolean>;

  constructor() {
    this.classApplied = new BehaviorSubject<boolean>(false);
  }

  toggleClass() : void{
    this.classApplied.next(!this.classApplied.value);
  }
  
  getValue(): Observable<boolean> {
    return this.classApplied.asObservable();
  }
}
