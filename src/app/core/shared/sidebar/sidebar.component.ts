import { Component } from '@angular/core';
import { SidebarService } from './sidebar.service';

@Component({
  selector: 'sl-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent {
  
  classApplied = false;

  constructor(private sidebarService: SidebarService) {
  }

  ngOnInit() {
    this.sidebarService.getValue().subscribe((value) => {
      this.classApplied = value;
    });
  }
  
  onClickMenu() {
    this.sidebarService.toggleClass();
  }

}

