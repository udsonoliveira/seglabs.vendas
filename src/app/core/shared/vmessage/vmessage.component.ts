import { Component, Input } from '@angular/core';

@Component({
    selector: 'sl-vmessage',
    templateUrl: './vmessage.component.html'
})
export class VMessageComponent {

    @Input() text = '';
 }