import { Component, OnInit } from '@angular/core';  
import { FormBuilder, Validators, FormGroup } from '@angular/forms';  
import { Observable } from 'rxjs'; 
import { DatePipe } from '@angular/common';

import { Venda } from 'src/app/venda/venda';
import { Vendedor } from 'src/app/vendedor/vendedor';
import { VendaService } from 'src/app/venda/venda.service';
import { VendedorService } from 'src/app/vendedor/vendedor.service';

@Component({
  selector: 'sl-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [DatePipe]
})
export class HomeComponent implements OnInit{
  allVendas: Observable<Venda[]>;  
  allVendedores: Observable<Vendedor[]>;
  constructor(private formbuilder: FormBuilder, private vendaService:VendaService, private vendedorService: VendedorService, private datePipe: DatePipe) { }  
  
  ngOnInit() {  
    this.loadAllVendas(); 
    this.loadAllVendedores();  
  }  

  loadAllVendas() {  
    this.allVendas = this.vendaService.getAllVendas('', this.datePipe.transform(new Date(), 'yyyy-MM-dd'));
  }
  
  loadAllVendedores() {
    this.allVendedores = this.vendedorService.getAllVendedores();
  }

}
