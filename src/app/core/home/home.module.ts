import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule }  from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';

import { HomeComponent } from './home.component';
import { VMessageModule } from '../shared/vmessage/vmessage.module';

@NgModule({
    declarations: [ 
        HomeComponent
    ],
    imports: [ 
        CommonModule, 
        FormsModule,
        ReactiveFormsModule,
        VMessageModule,
        RouterModule,
        MatToolbarModule,
        MatCardModule
    ],
    providers: [ ]
})
export class HomeModule { }