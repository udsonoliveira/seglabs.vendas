export class Produto {
    id: number;
    pro_descricao: string;
    pro_valor: number;
    pro_estoque: number;
  }