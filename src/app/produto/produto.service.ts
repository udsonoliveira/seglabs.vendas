import { Injectable } from '@angular/core';  
import { HttpClient } from '@angular/common/http';  
import { HttpHeaders } from '@angular/common/http';  
import { Observable } from 'rxjs';  
import { Produto } from './produto';

 @Injectable({  
  providedIn: 'root'  
})  
  
export class ProdutoService {  
  url = 'https://localhost:44321'; 

  constructor(private http: HttpClient) { }  

  getAllProdutos(): Observable<Produto[]> {  
    return this.http.get<Produto[]>(this.url + '/Api/Produto/');  
  }  
  getProdutoById(produtoId: string): Observable<Produto> {  
    return this.http.get<Produto>(this.url + '/Api/Produto?id=' + produtoId);  
  }  
  createProduto(produto: Produto): Observable<Produto> {  
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
    return this.http.post<Produto>(this.url + '/Api/Produto/',  
    JSON.stringify(produto), httpOptions);
  }  
  updateProduto(produto: Produto): Observable<Produto> {  
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
    return this.http.put<Produto>(this.url + '/Api/Produto/',  
    JSON.stringify(produto), httpOptions);  
  }  
}  