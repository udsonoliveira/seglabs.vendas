import { Component, OnInit } from '@angular/core';  
import { FormBuilder, Validators, FormGroup } from '@angular/forms';  
import { Observable } from 'rxjs';  
import { ProdutoService } from './produto.service';  
import { Produto } from './produto';  
  
@Component({  
  selector: 'sl-produto',  
  templateUrl: './produto.component.html',
  styleUrls: ['./produto.component.css']
})  
export class ProdutoComponent implements OnInit {  
  dataSaved = false;  
  produtoForm: FormGroup;  
  allProdutos: Observable<Produto[]>;  
  produtoIdUpdate = 0;  
  message = null;  
  
  constructor(private formbuilder: FormBuilder, private produtoService:ProdutoService) { }  
  
  ngOnInit() {  
    this.produtoForm = this.formbuilder.group({  
      id: [''],  
      pro_descricao: ['', [Validators.required]],
      pro_valor:['', [Validators.required]],
      pro_estoque:['',[Validators.required]]
    });  
    this.loadAllProdutos();  
  }  

  loadAllProdutos() {  
    this.allProdutos = this.produtoService.getAllProdutos();  
  }  

  onFormSubmit() {  
    this.dataSaved = false;  
    const produto = this.produtoForm.value;  
    this.CreateProduto(produto);  
    this.produtoForm.reset();  
  }  

  loadProdutoToEdit(produtoId: string) {  
      this.produtoService.getProdutoById(produtoId).subscribe(produto => { 
        this.message = null;  
        this.dataSaved = false;  
        this.produtoIdUpdate = produto[0].id;  
        this.produtoForm.controls['id'].setValue(produto[0].id) ;
        this.produtoForm.controls['pro_descricao'].setValue(produto[0].pro_descricao); 
        this.produtoForm.controls['pro_valor'].setValue(produto[0].pro_valor); 
        this.produtoForm.controls['pro_estoque'].setValue(produto[0].pro_estoque);        
      });  
  
  }  
  CreateProduto(produto: Produto) {  
    if (this.produtoIdUpdate == null || this.produtoIdUpdate <= 0) {
      produto.id = 0;
      this.produtoService.createProduto(produto).subscribe(  
        () => {  
          this.dataSaved = true;  
          this.message = 'Registro salvo com sucesso';  
          this.loadAllProdutos();  
          this.produtoIdUpdate = 0;  
          this.produtoForm.reset();  
        }  
      );  
    } else {  
      produto.id = this.produtoIdUpdate;  
      this.produtoService.updateProduto(produto).subscribe(() => {  
        this.dataSaved = true;  
        this.message = 'Registro atualizado com sucesso';  
        this.loadAllProdutos();  
        this.produtoIdUpdate = 0;  
        this.produtoForm.reset();  
      });  
    }  
  }   
    
  resetForm() {  
    this.produtoForm.reset();  
    this.message = null;  
    this.dataSaved = false;  
  }  
}  