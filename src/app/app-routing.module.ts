import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './core/home/home.component';
import { VendedorComponent } from './vendedor/vendedor.component';
import { ProdutoComponent } from './produto/produto.component';
import { VendaComponent } from './venda/venda.component';
import { VendaItemComponent } from './vendaitem/vendaitem.component';
import { NotFoundComponent } from './core/errors/notfound/notfound.component'

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'home'
    },
    { 
        path: 'home',
        component: HomeComponent
    },
    { 
        path: 'vendedor',
        component: VendedorComponent
    },
    { 
        path: 'produto',
        component: ProdutoComponent
    },
    { 
        path: 'venda',
        component: VendaComponent
    },
    { 
        path: 'venda/vendaitem',
        component: VendaItemComponent
    },
    {
        path: '**',
        component: NotFoundComponent
    }
];

@NgModule({
    imports: [ 
        RouterModule.forRoot(routes, { useHash: true } ) 
    ],
    exports: [ RouterModule ]
})
export class AppRoutingModule { }

