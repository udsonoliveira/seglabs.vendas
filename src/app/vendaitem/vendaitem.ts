import { Produto } from '../produto/produto';

export class VendaItem {
    id: number;
    vendaId: number;
    venIte_produto: Produto;
    produtoId: number;
    venIte_quantidade: number;
    venIte_subtotal: number;
}