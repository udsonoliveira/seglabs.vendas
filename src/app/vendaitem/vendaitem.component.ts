import { Component, OnInit, ViewEncapsulation } from '@angular/core';  
import { FormBuilder, Validators, FormGroup } from '@angular/forms';  
import { Observable } from 'rxjs'; 

import { VendaItemService } from './vendaitem.service';  
import { VendaItem } from './vendaitem'; 
import { Produto } from '../produto/produto';
import { ProdutoService } from '../produto/produto.service';
  
@Component({  
  selector: 'sl-vendaitem',  
  templateUrl: './vendaitem.component.html',
  styleUrls: ['./vendaitem.component.css'],
  encapsulation: ViewEncapsulation.None
})  
export class VendaItemComponent implements OnInit {  
  dataSaved = false;  
  vendaItemForm: FormGroup;  
  allVendaItens: Observable<VendaItem[]>;
  allProdutos: Observable<Produto[]>;
  vendaItemIdUpdate = 0;
  vendaId = 0;
  message = null;  
  
  constructor(private formbuilder: FormBuilder, private vendaItemService:VendaItemService, private produtoService: ProdutoService) { }  
  
  ngOnInit() {  
    this.vendaItemForm = this.formbuilder.group({  
      id: [''],
      vendaId: ['',[Validators.required]],
      produtoId: ['', [Validators.required]],
      venIte_quantidade: ['',[Validators.required]],
      venIte_subtotal: ['']
    });  
    this.loadAllVendaItens();
    this.loadAllProdutos();   
  }  

  loadAllVendaItens() {  
    this.allVendaItens = this.vendaItemService.getAllVendaItens(this.vendaId.toString());
  }
  
  loadAllProdutos() {
    this.allProdutos = this.produtoService.getAllProdutos();
  }
  
  onFormSubmit() {  
    this.dataSaved = false;  
    const vendaItem = this.vendaItemForm.value;  
    this.CreateVendaItem(vendaItem);  
    this.vendaItemForm.reset();  
  }  

  loadVendaItemToEdit(vendaitemId: string) {  
      this.vendaItemService.getVendaItemById(this.vendaId.toString(), vendaitemId).subscribe(vendaitem => {
        this.message = null;  
        this.dataSaved = false;  
        this.vendaItemIdUpdate = vendaitem[0].id;  
        this.vendaItemForm.controls['id'].setValue(vendaitem[0].id) ;
        this.vendaItemForm.controls['vendaId'].setValue(vendaitem[0].vendaId); 
        this.vendaItemForm.controls['produtoId'].setValue(vendaitem[0].produtoId); 
        this.vendaItemForm.controls['venIte_quantidade'].setValue(vendaitem[0].venIte_quantidade); 
        this.vendaItemForm.controls['venIte_subtotal'].setValue(vendaitem[0].venIte_subtotal);        
      });  
  
  }  

  CreateVendaItem(vendaItem: VendaItem) {  
    if (this.vendaItemIdUpdate == null || this.vendaItemIdUpdate <= 0) {
      vendaItem.id = 0;
      this.vendaItemService.createVendaItem(vendaItem).subscribe(  
        () => {  
          this.dataSaved = true;  
          this.message = 'Registro salvo com sucesso';  
          this.loadAllVendaItens();  
          this.vendaItemIdUpdate = 0;  
          this.vendaItemForm.reset();  
        }  
      );  
    } else {  
      vendaItem.id = this.vendaItemIdUpdate;  
      this.vendaItemService.updateVendaitem(vendaItem).subscribe(() => {  
        this.dataSaved = true;  
        this.message = 'Registro atualizado com sucesso';  
        this.loadAllVendaItens();  
        this.vendaItemIdUpdate = 0;  
        this.vendaItemForm.reset();  
      });  
    }  
  }   
    
  resetForm() {  
    this.vendaItemForm.reset();  
    this.message = null;  
    this.dataSaved = false;  
  }  
}  