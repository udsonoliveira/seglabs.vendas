import { Injectable } from '@angular/core';  
import { HttpClient } from '@angular/common/http';  
import { HttpHeaders } from '@angular/common/http';  
import { Observable } from 'rxjs';  
import { VendaItem } from './vendaitem';

 @Injectable({  
  providedIn: 'root'  
})  
  
export class VendaItemService {  
  url = 'https://localhost:44321'; 

  constructor(private http: HttpClient) { }  

  getAllVendaItens(vendaId: string): Observable<VendaItem[]> {  
    return this.http.get<VendaItem[]>(this.url + '/Api/VendaItem?vendaId=' + vendaId);  
  }  
  getVendaItemById(vendaId: string, vendaItemId: string): Observable<VendaItem> {  
    return this.http.get<VendaItem>(this.url + '/Api/VendaItem?vendaId=' + vendaId + '&id=' + vendaItemId);  
  }  
  createVendaItem(vendaItem: VendaItem): Observable<VendaItem> {  
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
    return this.http.post<VendaItem>(this.url + '/Api/VendaItem/',  
    vendaItem, httpOptions);
  }  
  updateVendaitem(vendaItem: VendaItem): Observable<VendaItem> {  
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
    return this.http.put<VendaItem>(this.url + '/Api/VendaItem/',  
    vendaItem, httpOptions);  
  }

  deleteVendaitem(vendaItem: VendaItem): Observable<VendaItem> {  
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
    return this.http.delete<VendaItem>(this.url + '/Api/VendaItem?vendaId=' + vendaItem.vendaId + '&id=' + vendaItem.id,  
    httpOptions);  
  }
}  