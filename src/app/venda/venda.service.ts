import { Injectable } from '@angular/core';  
import { HttpClient } from '@angular/common/http';  
import { HttpHeaders } from '@angular/common/http';  
import { Observable } from 'rxjs';  
import { Venda } from './venda';

 @Injectable({  
  providedIn: 'root'  
})  
  
export class VendaService {  
  url = 'https://localhost:44321'; 

  constructor(private http: HttpClient) { }  

  getAllVendas(vendedorId : string, ven_data: string): Observable<Venda[]> {  
    return this.http.get<Venda[]>(this.url + '/Api/Venda' + ((vendedorId.length > 0) ? '?vendedorId=' + vendedorId : '?vendedorId=0') + ((ven_data.length > 0) ? '&Ven_data=' + ven_data : ''));  
  }  
  getVendaById(vendaId: string): Observable<Venda> {  
    return this.http.get<Venda>(this.url + '/Api/Venda?id=' + vendaId);  
  }  
  createVenda(venda: Venda): Observable<Venda> {  
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
    return this.http.post<Venda>(this.url + '/Api/Venda/',  
    venda, httpOptions);
  }  
  updateVenda(venda: Venda): Observable<Venda> {  
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
    return this.http.put<Venda>(this.url + '/Api/Venda/',  
    venda, httpOptions);  
  }  
}  