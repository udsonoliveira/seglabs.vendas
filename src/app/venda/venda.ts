import { Vendedor } from '../vendedor/vendedor';
import { VendaItem } from '../vendaitem/vendaitem';

export class Venda {
    id: number;
    ven_vendedor: Vendedor;
    vendedorId: number;
    ven_data: Date;
    ven_total: number;
    ven_finalizada: boolean;
    ven_obs: string;
}
