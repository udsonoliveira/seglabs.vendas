import { Component, OnInit } from '@angular/core';  
import { FormBuilder, Validators, FormGroup } from '@angular/forms';  
import { Observable } from 'rxjs'; 

import { VendaService } from './venda.service';  
import { Venda } from './venda';  
import { Vendedor } from '../vendedor/vendedor';
import { VendedorService } from '../vendedor/vendedor.service';
  
@Component({  
  selector: 'sl-venda',  
  templateUrl: './venda.component.html',
  styleUrls: ['./venda.component.css']
})  
export class VendaComponent implements OnInit {  
  dataSaved = false;  
  vendaForm: FormGroup;  
  allVendas: Observable<Venda[]>;  
  allVendedores: Observable<Vendedor[]>;
  vendaIdUpdate = 0;
  message = null;  
  
  constructor(private formbuilder: FormBuilder, private vendaService:VendaService, private vendedorService: VendedorService) { }  
  
  ngOnInit() {  
    this.vendaForm = this.formbuilder.group({  
      id: [''],  
      vendedorId: ['', [Validators.required]],
      ven_data:[''],
      ven_total: [''],
      ven_finalizada: [''],
      ven_obs: ['']
    });  
    this.loadAllVendas(); 
    this.loadAllVendedores();  
  }  

  loadAllVendas() {  
    this.allVendas = this.vendaService.getAllVendas('','');
  }
  
  loadAllVendedores() {
    this.allVendedores = this.vendedorService.getAllVendedores();
  }

  onFormSubmit() {  
    this.dataSaved = false;  
    const venda = this.vendaForm.value;  
    this.CreateVenda(venda);  
    this.vendaForm.reset();  
  }  

  loadVendaToEdit(vendaId: string) {  
      this.vendaService.getVendaById(vendaId).subscribe(venda => {
        this.message = null;  
        this.dataSaved = false;  
        this.vendaIdUpdate = venda[0].id;  
        this.vendaForm.controls['id'].setValue(venda[0].id) ;
        this.vendaForm.controls['vendedorId'].setValue(venda[0].vendedorId); 
        this.vendaForm.controls['ven_data'].setValue(venda[0].ven_data); 
        this.vendaForm.controls['ven_total'].setValue(venda[0].ven_total); 
        this.vendaForm.controls['ven_finalizada'].setValue(venda[0].ven_finalizada); 
        this.vendaForm.controls['ven_obs'].setValue(venda[0].ven_obs);        
      });  
  
  }  

  CreateVenda(venda: Venda) {  
    if (this.vendaIdUpdate == null || this.vendaIdUpdate <= 0) {
      venda.id = 0;
      venda.ven_total = 0;
      venda.ven_finalizada = false;
      venda.ven_data = new Date();
      this.vendaService.createVenda(venda).subscribe(  
        () => {  
          this.dataSaved = true;  
          this.message = 'Registro salvo com sucesso';  
          this.loadAllVendas();  
          this.vendaIdUpdate = 0;  
          this.vendaForm.reset();  
        }  
      );  
    } else {  
      venda.id = this.vendaIdUpdate;  
      this.vendaService.updateVenda(venda).subscribe(() => {  
        this.dataSaved = true;  
        this.message = 'Registro atualizado com sucesso';  
        this.loadAllVendas();  
        this.vendaIdUpdate = 0;  
        this.vendaForm.reset();  
      });  
    }  
  }   
    
  resetForm() {  
    this.vendaForm.reset();  
    this.message = null;  
    this.dataSaved = false;  
  }  
}  