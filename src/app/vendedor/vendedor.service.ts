import { Injectable } from '@angular/core';  
import { HttpClient } from '@angular/common/http';  
import { HttpHeaders } from '@angular/common/http';  
import { Observable } from 'rxjs';  
import { Vendedor } from './vendedor';

 @Injectable({  
  providedIn: 'root'  
})  
  
export class VendedorService {  
  url = 'https://localhost:44321'; 

  constructor(private http: HttpClient) { }  

  getAllVendedores(): Observable<Vendedor[]> {  
    return this.http.get<Vendedor[]>(this.url + '/Api/Vendedor/');  
  }  
  getVendedorById(vendedorId: string): Observable<Vendedor> {  
    return this.http.get<Vendedor>(this.url + '/Api/Vendedor?id=' + vendedorId);  
  }  
  createVendedor(vendedor: Vendedor): Observable<Vendedor> {  
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
    return this.http.post<Vendedor>(this.url + '/Api/Vendedor/',  
    vendedor, httpOptions);
  }  
  updateVendedor(vendedor: Vendedor): Observable<Vendedor> {  
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
    return this.http.put<Vendedor>(this.url + '/Api/Vendedor/',  
    vendedor, httpOptions);  
  }  
}  