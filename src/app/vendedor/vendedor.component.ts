import { Component, OnInit } from '@angular/core';  
import { FormBuilder, Validators, FormGroup } from '@angular/forms';  
import { Observable } from 'rxjs';  
import { VendedorService } from './vendedor.service';  
import { Vendedor } from './vendedor';  
  
@Component({  
  selector: 'sl-vendedor',  
  templateUrl: './vendedor.component.html',
  styleUrls: ['./vendedor.component.css']
})  
export class VendedorComponent implements OnInit {  
  dataSaved = false;  
  vendedorForm: FormGroup;  
  allVendedores: Observable<Vendedor[]>;  
  vendedorIdUpdate = 0;  
  message = null;  
  
  constructor(private formbuilder: FormBuilder, private vendedorService:VendedorService) { }  
  
  ngOnInit() {  
    this.vendedorForm = this.formbuilder.group({  
      id: [''],  
      ven_nome: ['', [Validators.required]]
    });  
    this.loadAllVendedores();  
  }  

  loadAllVendedores() {  
    this.allVendedores = this.vendedorService.getAllVendedores();  
  }  

  onFormSubmit() {  
    this.dataSaved = false;  
    const vendedor = this.vendedorForm.value;  
    this.CreateVendedor(vendedor);  
    this.vendedorForm.reset();  
  }  

  loadVendedorToEdit(vendedorId: string) {  
      this.vendedorService.getVendedorById(vendedorId).subscribe(vendedor => { 
        this.message = null;  
        this.dataSaved = false;  
        this.vendedorIdUpdate = vendedor[0].id;  
        this.vendedorForm.controls['id'].setValue(vendedor[0].id) ;
        this.vendedorForm.controls['ven_nome'].setValue(vendedor[0].ven_nome);        
      });  
  
  }  
  CreateVendedor(vendedor: Vendedor) {  
    if (this.vendedorIdUpdate == null || this.vendedorIdUpdate <= 0) {
      vendedor.id = 0;
      this.vendedorService.createVendedor(vendedor).subscribe(  
        () => {  
          this.dataSaved = true;  
          this.message = 'Registro salvo com sucesso';  
          this.loadAllVendedores();  
          this.vendedorIdUpdate = 0;  
          this.vendedorForm.reset();  
        }  
      );  
    } else {  
      vendedor.id = this.vendedorIdUpdate;  
      this.vendedorService.updateVendedor(vendedor).subscribe(() => {  
        this.dataSaved = true;  
        this.message = 'Registro atualizado com sucesso';  
        this.loadAllVendedores();  
        this.vendedorIdUpdate = 0;  
        this.vendedorForm.reset();  
      });  
    }  
  }   
    
  resetForm() {  
    this.vendedorForm.reset();  
    this.message = null;  
    this.dataSaved = false;  
  }  
}  